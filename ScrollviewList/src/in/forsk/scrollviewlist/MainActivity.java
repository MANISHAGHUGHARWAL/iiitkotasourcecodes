package in.forsk.scrollviewlist;

import android.app.Activity;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class MainActivity extends Activity {
	private final static String TAG = MainActivity.class.getSimpleName();
	// View Group to add the child views
	LinearLayout listLinerLayout;

	// Data Model
	String[] data_array = new String[] { "Dr. Bharavi Mishra", "Mr. Dinesh Khandelwal", "Mr. Mukesh K Jadon", "Dr. Poonam", "Dr. PRAVEEN KUMAR", "Dr. Preety Singh", "Dr. Rajbir Kaur",
			"Dr. Rajni Aron", "Dr. RATNADIP ADHIKARI", "Prof. Ravi Prakash Gorthi", "Dr. Sakthi Balan Muthiah", "Mrs. Sonam Nahar", "Dr. Subrat Kumar Dash", "Mr. Sunil Kumar", "Dr. VIBHOR KANT",
			"Mr. Vikas Bajpai" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// creating reference to the linear layout
		listLinerLayout = (LinearLayout) findViewById(R.id.listLinerLayout);

		// Filling data model into linear layout manually
		// iterating the array, and creating text view object dynamically and
		// adding on to the layout(View group)
		for (int i = 0; i < data_array.length; i++) {

			// Creating text view obj
			TextView textView = new TextView(this);

			// setting the layout properties
			textView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			// Set Text
			textView.setText(data_array[i]);
			textView.setBackgroundColor(android.R.color.darker_gray);
			textView.setPadding(30, 30, 30, 30);// in pixels (left, top, right,
			textView.setTextSize(20);

			// Adding textview into the layout
			listLinerLayout.addView(textView);

		}

		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Log.d(TAG, "getNativeHeapAllocatedSize - " + Debug.getNativeHeapAllocatedSize());
				Log.d(TAG, "getNativeHeapSize - " + Debug.getNativeHeapSize());
				Log.d(TAG, "getNativeHeapFreeSize - " + Debug.getNativeHeapFreeSize());
			}
		}, 2000);
		
	}
}
